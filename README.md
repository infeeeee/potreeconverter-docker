# potreeconverter-docker

Docker image for [potreeconverter](https://potree.github.io/index.html)

## Usage

### Environment variables

| Variable name          | Default value  | More info                                                                  |
| ---------------------- | -------------- | -------------------------------------------------------------------------- |
| `POTREE_ENCODING`      | `UNCOMPRESSED` | Other option: `BROTLI`                                                     |
| `POTREE_METHOD`        | `poisson`      | Other options: `poisson_average`, `random`                                 |
| `POTREE_EXTRA_OPTIONS` | `""`           | All other command line options, check potree documentation                 |
| `OVERWRITE`            | `TRUE`         | Change to anything else to disable converting already converted clouds     |
| `REMOVE_INDEX`         | `TRUE`         | Remove index.html from output dir. Useful for removing default apache file |

### Conversion with `docker run` command

Pull the image:

```shell
docker pull registry.gitlab.com/infeeeee/potreeconverter-docker:latest
```

See possible tags here: [Gitlab Container Registry](https://gitlab.com/infeeeee/potreeconverter-docker/container_registry/3706312)


Mount directory containing `.las`/`.laz` files to `/data/input`. Mount empty output directory to `/data/output`.

Minimal example:

```shell
docker run -it --rm -u $UID -v $PWD/input:/data/input:ro -v $PWD/output:/data/output:rw potree-converter:latest
```

Example with all variables:
```shell
docker run -it --rm \
    -u $UID \
    -v $PWD/input:/data/input:ro \
    -v $PWD/output:/data/output:rw \
    --env POTREE_ENCODING=UNCOMPRESSED \
    --env POTREE_METHOD=poisson \
    --env POTREE_EXTRA_OPTIONS="--projection" \
    --env OVERWRITE=FALSE \
    potree-converter:latest
```

### `docker compose` with webserver

This is a ready to use solution to convert and share pointclouds. Just mount directory containing `las` or `laz` files to `/data/input`, and after conversion the converted pointclouds are accessible on port `8080`.

```yaml
services:
  potree_converter:
    container_name: potree_converter
    image: registry.gitlab.com/infeeeee/potreeconverter-docker:latest
    volumes: 
      - /path/to/pointclouds:/data/input:ro
      - potree-output:/data/output:rw
  potree_server:
    container_name: potree_server
    image: httpd:latest
    volumes:
      - potree-output:/usr/local/apache2/htdocs
    ports:
      - "8080:80"
    restart: unless-stopped
    
volumes:
  potree-output:
```


## Build the image

```shell
docker build --pull --rm -f Dockerfile -t potree-converter:latest --build-arg VERSION=0.0.0 .
```